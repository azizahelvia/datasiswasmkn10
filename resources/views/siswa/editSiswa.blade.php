@extends('layouts.master')

@section('content')
        <h1>Ubah Data</h1>
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
           {{session('sukses')}}
        </div>
        @endif
            <div class="row">
            <div class="col-lg-12">
            <form action="/siswa/{{$siswa->id}}/updateData" method="POST">
            {{csrf_field()}}
            <div class="form-group">
                <label for="NamaLengkap">Nama Lengkap</label>
                <input name="nama_lengkap" type="text" class="form-control" id="namaLengkap" aria-describedby="NamaSiswa"
                placeholder="Nama Lengkap" value="{{$siswa->nama_lengkap}}">   
            </div>
            <div class="form-group">
                <label for="Kelas">Kelas</label>
                <input name="kelas" type="text" class="form-control" id="kelas" aria-describedby="KelasSiswa"
                placeholder="Kelas" value="{{$siswa->kelas}}">   
            </div>
            <div class="form-group">
                <label for="nomorAbsen">Nomor Absen</label>
                <input name="nomor_absen" type="number" class="form-control" id="NomorAbsen" aria-describedby="AbsenSiswa"
                placeholder="Nomor Absen" value="{{$siswa->nomor_absen}}">   
            </div>
            <div class="form-group">
                <label for="jenisKelamin">Jenis Kelamin</label>
                <select name="jenis_kelamin" class="form-control" id="JenisKelamin">
                <option value="Jenis Kelamin" @if($siswa->jenis_kelamin == 'Jenis Kelamin') selected @endif>Jenis Kelamin</option>
                <option value="Laki-laki"  @if($siswa->jenis_kelamin == 'Laki-laki') selected @endif>Laki-laki</option>
                <option value="Perempuan"  @if($siswa->jenis_kelamin == 'Perempuan') selected @endif>Perempuan</option>
                </select>
            </div>
            <div class="form-group">
                <label for="exampleFormControlSelect1">Agama</label>
                <select name="agama" class="form-control" id="Agama">
                <option value="Agama" @if($siswa->agama == 'Agama') selected @endif>Agama</option>
                <option value="Islam" @if($siswa->agama == 'Islam') selected @endif>Islam</option>
                <option value="Kristen" @if($siswa->agama == 'Kristen') selected @endif>Kristen</option>
                <option value="Katholik" @if($siswa->agama == 'Katholik') selected @endif>Katholik</option>
                <option value="Hindu" @if($siswa->agama == 'Hindu') selected @endif>Hindu</option>
                <option value="Buddha" @if($siswa->agama == 'Buddha') selected @endif>Buddha</option>
                <option value="Khonghuchu" @if($siswa->agama == 'Khonghuchu') selected @endif>Khonghuchu</option>
                </select>
            </div>
            <div class="form-group">
                <label for="inputAddress">Alamat</label>
                <input name="alamat" type="text" class="form-control" id="dataAlamat" placeholder="Alamat"
                value="{{$siswa->alamat}}">
            </div> 
            <button type="submit" class="btn btn-outline-info btn-sm">Perbarui</button>
                </form>
                </div>
            </div>

@endsection            