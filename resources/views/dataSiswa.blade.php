@extends('layouts.master')

@section('content')
        @if(session('sukses'))
        <div class="alert alert-success" role="alert">
           {{session('sukses')}}
        </div>
        @endif
            <div class="row">
                <div class="col-6">
                    <h1>Daftar Siswa</h1>
                </div>
                <div class="col-6">
                    <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal">
                    Tambah
                    </button>
                </div>
                <table class="table table-hover table-dark">
                <caption>Daftar Siswa SMK Negeri 10 Jakarta</caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">Nama Lengkap</th>
                    <th scope="col">Kelas</th>
                    <th scope="col">Nomor Absen</th>
                    <th scope="col">Jenis Kelamin</th>
                    <th scope ="col">Agama</th>
                    <th scope="col">Alamat</th>
                    <th scope ="col">Edit</th>
                </tr>
                </thead>
                @foreach($data_siswa as $siswa)
                <tr>
                    <td>{{$siswa->nama_lengkap}}</td>
                    <td>{{$siswa->kelas}}</td>
                    <td>{{$siswa->nomor_absen}}</td>
                    <td>{{$siswa->jenis_kelamin}}</td>
                    <td>{{$siswa->agama}}</td>
                    <td>{{$siswa->alamat}}</td>
                    <td>
                    <a href="/siswa/{{$siswa->id}}/editData" class="btn btn-outline-info btn-sm">Ubah</a>
                    <a href="/siswa/{{$siswa->id}}/deleteData" class="btn btn-outline-danger btn-sm"
                    onclick="return confirm('Apakah Ingin Menghapus Data ?')">Hapus</a>
                    </td>
                </tr>
                @endforeach
                </table>
            </div>
        </div>

    <!-- Modal -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Tambah Data</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
        <form action="/siswa/tambahData" method="POST">
            {{csrf_field()}}
        <div class="form-group">
            <label for="NamaLengkap">Nama Lengkap</label>
            <input name="nama_lengkap" type="text" class="form-control" id="namaLengkap" aria-describedby="NamaSiswa"
            placeholder="Nama Lengkap">   
        </div>
        <div class="form-group">
            <label for="Kelas">Kelas</label>
            <input name="kelas" type="text" class="form-control" id="kelas" aria-describedby="KelasSiswa"
            placeholder="Kelas">   
        </div>
        <div class="form-group">
            <label for="nomorAbsen">Nomor Absen</label>
            <input name="nomor_absen" type="number" class="form-control" id="NomorAbsen" aria-describedby="AbsenSiswa"
            placeholder="Nomor Absen">   
        </div>
        <div class="form-group">
            <label for="jenisKelamin">Jenis Kelamin</label>
            <select name="jenis_kelamin" class="form-control" id="JenisKelamin">
            <option>Jenis Kelamin</option>
            <option>Laki-laki</option>
            <option>Perempuan</option>
            </select>
        </div>
        <div class="form-group">
            <label for="exampleFormControlSelect1">Agama</label>
            <select name="agama" class="form-control" id="Agama">
            <option>Agama</option>
            <option>Islam</option>
            <option>Kristen</option>
            <option>Katholik</option>
            <option>Hindu</option>
            <option>Buddha</option>
            <option>Khonghuchu</option>
            </select>
        </div>
        <div class="form-group">
            <label for="inputAddress">Alamat</label>
            <input name="alamat" type="text" class="form-control" id="dataAlamat" placeholder="Alamat">
        </div>
        </div>

        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-primary btn-md">Simpan</button>
        </form>
        </div>
        </div>
    </div>
            
 @endsection   
    