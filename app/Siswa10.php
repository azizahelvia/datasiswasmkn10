<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Siswa10 extends Model
{
    protected $table = 'siswa';
    protected $fillable = ['nama_lengkap','kelas','nomor_absen','jenis_kelamin','agama','alamat'];
}
