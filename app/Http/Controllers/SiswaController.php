<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SiswaController extends Controller
{
    public function dataSiswa()
    {
        $data_siswa = \App\Siswa10::all();
        return view ('dataSiswa',['data_siswa' => $data_siswa]);
    }

    public function tambahData(Request $request)
    {
        \App\Siswa10::create($request->all());
        return redirect('/siswa')->with('sukses', 'Data Berhasil Ditambahkan');
    }

    public function editData($id)
    {
        $siswa = \App\Siswa10::find($id);
        return view('siswa/editSiswa',['siswa' => $siswa]);
    }

    public function updateData(Request $request,$id)
    {
        $siswa = \App\Siswa10::find($id);
        $siswa->update($request->all());
        return redirect('/siswa')->with('sukses','Data Berhasil Diperbarui');
    }

    public function deleteData($id)
    {
        $siswa = \App\Siswa10::find($id);
        $siswa->delete();
        return redirect('/siswa')->with('sukses','Data Berhasil Dihapus');
    }
}
