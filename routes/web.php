<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/siswa','SiswaController@dataSiswa');

Route::post('/siswa/tambahData','SiswaController@tambahData');

Route::get('/siswa/{id}/editData','SiswaController@editData');

Route::post('/siswa/{id}/updateData','SiswaController@updateData');

Route::get('/siswa/{id}/deleteData','SiswaController@deleteData');